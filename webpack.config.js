const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const mode = process.env.NODE_ENV;
const isDev = mode === 'development';
const SRC = path.resolve(__dirname, 'src');
const DIST = path.resolve(__dirname, 'dist');

module.exports = {
  entry: {
    main: path.join(SRC, 'index.tsx'),
  },
  output: {
    filename: '[name].js',
    path: DIST,
  },
  watch: isDev,
  mode: mode || 'production',
  module: {
    rules: [
      {
        test: /\.styl$/,
        use: [
          { loader: 'stylus-loader' },
          { loader: 'css-loader', options: { modules: true } },
          { loader: 'style-loader' },
        ],
      },
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        use: [
          {
            loader: 'ts-loader',
          },
        ],
      },
      {
        test: /\.jsx?$/,
        exclude: '/node_modules/',
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new HTMLWebpackPlugin({
      title: 'Typescript test',
      chunks: ['main'],
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
  },
  devServer: {
    contentBase: DIST,
    port: 4000,
    hot: true,
    inline: true
  },
};
